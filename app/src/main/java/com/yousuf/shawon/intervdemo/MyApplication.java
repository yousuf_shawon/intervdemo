package com.yousuf.shawon.intervdemo;

import android.app.Application;
import android.util.Log;
import com.yousuf.shawon.intervdemo.model.ClientResponse;
import com.yousuf.shawon.intervdemo.util.NetworkUtil;

/**
 * Created by Yousuf on 6/16/2017.
 */

public class MyApplication extends Application {

  public static ClientResponse responseData = null;
  private String TAG = getClass().getSimpleName();

  @Override public void onCreate() {
    super.onCreate();
    Log.d(TAG, "onCreate");


  }
}

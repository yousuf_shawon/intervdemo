package com.yousuf.shawon.intervdemo.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Yousuf on 6/15/2017.
 */

public class ClientResponse {

  @SerializedName("client") ArrayList<Client> clientList;
  @SerializedName("error") boolean error;
  @SerializedName("message") String message;

  public ArrayList<Client> getClientList() {
    return clientList;
  }

  public void setClientList(ArrayList<Client> clientList) {
    this.clientList = clientList;
  }

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override public String toString() {
    return "ClientResponse{"
        + "clientList="
        + clientList == null ? "null" : Arrays.toString(clientList.toArray())
        + ", error="
        + error
        + ", message='"
        + message
        + '\''
        + '}';
  }
}

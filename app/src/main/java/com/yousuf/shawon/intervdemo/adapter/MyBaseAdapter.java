package com.yousuf.shawon.intervdemo.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.yousuf.shawon.intervdemo.BR;
import com.yousuf.shawon.intervdemo.ClientActivity;
import com.yousuf.shawon.intervdemo.R;
import com.yousuf.shawon.intervdemo.intrf.ItemClickListener;
import com.yousuf.shawon.intervdemo.model.Client;
import com.yousuf.shawon.intervdemo.model.TagData;
import java.util.List;

/**
 * Created by Yousuf on 6/15/2017.
 */

public abstract class MyBaseAdapter extends RecyclerView.Adapter<MyBaseAdapter.MyViewHolder> {


  public ItemClickListener mListener;
  String TAG = MyBaseAdapter.class.getSimpleName();

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

    ViewDataBinding
        binding = DataBindingUtil.inflate(layoutInflater, getLayoutIdForType(viewType), parent, false);

    return getViewHolderForBinding(binding);
  }



  @Override
  public void onBindViewHolder(MyViewHolder holder, int position) {
    Object obj = getObjForPosition(position);
    holder.bind(obj);
  }

  public void setOnItemClickListener(ItemClickListener mListener){
    this.mListener = mListener;
  }


  protected abstract Object getObjForPosition(int position);

  protected abstract int getLayoutIdForType(int type);

  protected abstract MyViewHolder getViewHolderForBinding(ViewDataBinding binding);


  class MyViewHolder extends RecyclerView.ViewHolder{

    private final ViewDataBinding binding;

    public MyViewHolder(ViewDataBinding binding) {
      super(binding.getRoot());
      this.binding = binding;

    }

    public void bind(Object obj){
      binding.setVariable(BR.obj, obj);

      binding.getRoot().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          if (mListener != null) {
            mListener.onItemClick(binding.getRoot(), getAdapterPosition());
          }
        }
      });

      doCustomBinding(binding);
      binding.executePendingBindings();
    }


    public void doCustomBinding(ViewDataBinding binding){}
  }


}


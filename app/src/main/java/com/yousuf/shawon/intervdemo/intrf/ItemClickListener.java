package com.yousuf.shawon.intervdemo.intrf;

import android.view.View;

/**
 * Created by Yousuf on 6/16/2017.
 */

public interface ItemClickListener {
  public void onItemClick(View v, int position);
}

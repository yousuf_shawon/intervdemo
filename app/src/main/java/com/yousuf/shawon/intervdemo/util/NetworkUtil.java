package com.yousuf.shawon.intervdemo.util;

import android.util.Log;
import com.yousuf.shawon.intervdemo.BuildConfig;
import com.yousuf.shawon.intervdemo.MyApplication;
import com.yousuf.shawon.intervdemo.model.ClientResponse;
import com.yousuf.shawon.intervdemo.rest.ApiClient;
import com.yousuf.shawon.intervdemo.rest.ApiInterface;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yousuf on 6/17/2017.
 */

public class NetworkUtil {

  static  String TAG = NetworkUtil.class.getSimpleName();

  public static void getResponseAsync(){

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    Call<ClientResponse> call = apiService.getData(BuildConfig.API_KEY);


    call.enqueue(new Callback<ClientResponse>() {
      @Override
      public void onResponse(Call<ClientResponse> call, Response<ClientResponse> response) {
        int statusCode = response.code();
        Log.d(TAG,"Status Code: " + statusCode );

        if (response.isSuccessful()) {

          Log.d(TAG, "Got Response");
          MyApplication.responseData = response.body();
          if (MyApplication.responseData != null) {
            Log.d(TAG, "Response: " + MyApplication.responseData);
          }

        }else {
          Log.d(TAG, "Response is not successful");
        }
      }

      @Override public void onFailure(Call<ClientResponse> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.getMessage());
      }
    });

  }

  public static ClientResponse getResponseSync(){
    ClientResponse data = null;
    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    Call<ClientResponse> call = apiService.getData(BuildConfig.API_KEY);

    // getting data by synchronous method

    try {
      Response<ClientResponse> response =  call.execute();
       data = response.body();
      if (data == null) {
        Log.d(TAG, "Response data is null");
      }else {
        Log.d(TAG, "Response: " + data);
        MyApplication.responseData = data;
      }
    } catch (IOException e) {
      // e.printStackTrace();
      Log.e(TAG, e.toString());
    }

    return data;
  }

}

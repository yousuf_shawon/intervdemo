package com.yousuf.shawon.intervdemo.adapter;

import com.yousuf.shawon.intervdemo.intrf.ItemClickListener;

/**
 * Created by Yousuf on 6/15/2017.
 */

public abstract class SingleLayoutAdapter extends MyBaseAdapter {


  private final int layoutId;

  public SingleLayoutAdapter(int layoutId ) {
    this.layoutId = layoutId;

  }

  @Override
  protected int getLayoutIdForType(int position) {
    return layoutId;
  }



}

package com.yousuf.shawon.intervdemo;

import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.yousuf.shawon.intervdemo.adapter.ClientAdapter;
import com.yousuf.shawon.intervdemo.databinding.ActivityClientBinding;
import com.yousuf.shawon.intervdemo.databinding.TextViewTagBinding;
import com.yousuf.shawon.intervdemo.intrf.ItemClickListener;
import com.yousuf.shawon.intervdemo.loader.ClientListLoader;
import com.yousuf.shawon.intervdemo.model.Client;
import com.yousuf.shawon.intervdemo.model.ClientResponse;
import com.yousuf.shawon.intervdemo.model.TagData;
import com.yousuf.shawon.intervdemo.util.ConnectionDetector;
import com.yousuf.shawon.intervdemo.util.Util;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Objects;

public class ClientActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ClientResponse>,
    ItemClickListener {

  private static int CLIENT_LOADER_ID = 1;
  private static String TAG = ClientActivity.class.getSimpleName();

  ClientAdapter mAdapter;
  RecyclerView mRecyclerView;

  ConnectionDetector cd;



  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setContentView(R.layout.activity_client);
    Log.d(TAG, "onCreate");

    cd = new ConnectionDetector(this);

    ActivityClientBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_client);
    mRecyclerView = activityBinding.recycleView;
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    mAdapter = new ClientAdapter(new ArrayList<Client>(), this);
    mRecyclerView.setAdapter(mAdapter);


    getSupportLoaderManager().initLoader(CLIENT_LOADER_ID, null, this);

    if (!cd.isConnectedToInternet()) {
      Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
    }

  }

  @Override public Loader<ClientResponse> onCreateLoader(int id, Bundle args) {
    Log.d(TAG, "onCreateLoader");

    return new ClientListLoader(getApplicationContext());
  }

  @Override public void onLoadFinished(Loader<ClientResponse> loader, ClientResponse data) {
    Log.d(TAG, "onLoadFinish");

    mAdapter.clear();

    if (data != null) {
      mAdapter = new ClientAdapter(data.getClientList(), this);
      mRecyclerView.setAdapter(mAdapter);

    }else {
      Log.d(TAG, "Response data is null");
    }


  }

  @Override public void onLoaderReset(Loader<ClientResponse> loader) {
    Log.d(TAG, "onLoaderReset");
   // mAdapter.clear();
  }

  @Override public void onItemClick(View v, int position) {
    Log.d(TAG, "Click at " + position);
    View bottomView = v.findViewById(R.id.linearLayoutBottomBar);
    if (bottomView != null) {
      int tagValue = (int) bottomView.getTag();
      Log.d(TAG, "tag: " + tagValue);
      if (tagValue==1) {
        bottomView.setVisibility(View.VISIBLE);
        bottomView.setTag(2);
        Util.expand(bottomView);

      }else {
        bottomView.setVisibility(View.INVISIBLE);
        Util.collapse(bottomView);

        bottomView.setTag(1);

      }


    }
  }



  @BindingAdapter("loadIcon")
  public static void loadImageIcon(final ImageView imageView, String url){
    Log.d(TAG, "loading image ");
    if (url == null) {
      return;
    }

    Glide.with(imageView.getContext())
        .load(url)
        .asBitmap()
        .fitCenter()
        .into(new BitmapImageViewTarget(imageView){
          @Override protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(imageView.getResources(), resource);
            circularBitmapDrawable.setCircular(true);
            imageView.setImageDrawable(circularBitmapDrawable);
          }
        });

  }




}

package com.yousuf.shawon.intervdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.yousuf.shawon.intervdemo.DashboardActivity;
import com.yousuf.shawon.intervdemo.MainActivity;

/**
 * Created by Yousuf on 6/3/2017.
 */

public class SplashFragment extends Fragment {

  private static String TAG = SplashFragment.class.getSimpleName();

  public interface TaskCallback{
    public void onFinish();
  }


  private TaskCallback mCallbacks;
  /**
   * Hold a reference to the parent Activity so we can report the
   * task's current progress and results. The Android framework
   * will pass us a reference to the newly created Activity after
   * each configuration change.
   */
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    if (context instanceof Activity){
      Activity activity = (Activity) context;
      mCallbacks = (TaskCallback) activity;
    }

  }


  /**
   * This method will only be called once when the retained
   * Fragment is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Retain this fragment across configuration changes.
    setRetainInstance(true);

    // Create and execute the background task.

    Handler mainHandler = new Handler(getActivity().getMainLooper());
    Log.d(TAG, "count start");
    mainHandler.postDelayed(new Runnable() {
      @Override public void run() {
        if (mCallbacks != null) {
          mCallbacks.onFinish();
        }
        Log.d(TAG, "Count finish");
      }
    }, 1500);


  }


  /**
   * Set the callback to null so we don't accidentally leak the
   * Activity instance.
   */
  @Override
  public void onDetach() {
    super.onDetach();
    mCallbacks = null;
  }


}

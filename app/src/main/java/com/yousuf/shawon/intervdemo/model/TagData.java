package com.yousuf.shawon.intervdemo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yousuf on 6/15/2017.
 */

public class TagData {

  @SerializedName("tag")
  String tag;
  @SerializedName("url")
  String url;

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override public String toString() {
    return "TagData{" + "tag='" + tag + '\'' + ", url='" + url + '\'' + '}';
  }
}

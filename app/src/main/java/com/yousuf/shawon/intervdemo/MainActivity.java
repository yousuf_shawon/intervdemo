package com.yousuf.shawon.intervdemo;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.yousuf.shawon.intervdemo.fragment.SplashFragment;
import com.yousuf.shawon.intervdemo.util.NetworkUtil;

public class MainActivity extends AppCompatActivity implements SplashFragment.TaskCallback,
    Animation.AnimationListener {

  private static final String TAG_SPLASH_FRAGMENT = "splash_fragment";

  private SplashFragment mSplashFragment;
  ImageView imageViewLogo, imageViewBottom;
  Animation animFadeIn, animMoveRight;

  String TAG = MainActivity.class.getSimpleName();


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Log.d(TAG, "onCreate");
    // getting response in Background Thread
    NetworkUtil.getResponseAsync();


    imageViewLogo = (ImageView) findViewById(R.id.imageViewLogo);
    imageViewBottom = (ImageView) findViewById(R.id.imageViewBottom);

    animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
    animMoveRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_right);

  //  FragmentManager fragmentManager = getSupportFragmentManager();
  //  mSplashFragment = (SplashFragment) fragmentManager.findFragmentByTag(TAG_SPLASH_FRAGMENT);

    // If the Fragment is non-null, then it is currently being
    // retained across a configuration change.
    //if (mSplashFragment == null) {
    //  mSplashFragment = new SplashFragment();
    //  fragmentManager.beginTransaction().add(mSplashFragment, TAG_SPLASH_FRAGMENT).commit();
    //}


    animFadeIn.setAnimationListener(this);
    animMoveRight.setAnimationListener(this);

    Log.d(TAG, "Starting Animation");
    imageViewLogo.startAnimation(animFadeIn);




  }


  private void navigateToNextActivity(){
     startActivity(new Intent(MainActivity.this, DashboardActivity.class));
     finish();
  }


  @Override
  public void onFinish() {
    Log.d(TAG, "starting next activity");
    navigateToNextActivity();
  }

  @Override public void onAnimationStart(Animation animation) {

  }

  @Override public void onAnimationEnd(Animation animation) {
    if (animation == animFadeIn) {
      imageViewBottom.startAnimation(animMoveRight);
      return;
    }

    if (animation == animMoveRight) {

      navigateToNextActivity();
    }


  }

  @Override public void onAnimationRepeat(Animation animation) {

  }
}

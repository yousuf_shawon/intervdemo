package com.yousuf.shawon.intervdemo.adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.yousuf.shawon.intervdemo.ClientActivity;
import com.yousuf.shawon.intervdemo.R;
import com.yousuf.shawon.intervdemo.databinding.TextViewTagBinding;
import com.yousuf.shawon.intervdemo.intrf.ItemClickListener;
import com.yousuf.shawon.intervdemo.model.Client;
import com.yousuf.shawon.intervdemo.model.ClientResponse;
import com.yousuf.shawon.intervdemo.model.TagData;
import java.util.List;

/**
 * Created by Yousuf on 6/15/2017.
 */

public class ClientAdapter extends SingleLayoutAdapter {

  List<Client> clientList;

  EventHandler eventHandler;
  String TAG = getClass().getSimpleName();

  public ClientAdapter(List<Client> clientList, ItemClickListener mListener) {
    super(R.layout.row_item);
    this.clientList = clientList;
    setOnItemClickListener(mListener);
    eventHandler = new EventHandler();
  }

  @Override protected Object getObjForPosition(int position) {
    return clientList.get(position);
  }

  @Override protected MyViewHolder getViewHolderForBinding(ViewDataBinding binding) {
    return new ClintViewHolder(binding);
  }

  @Override public int getItemCount() {
    return clientList.size();
  }

  public void clear(){
    if (clientList != null) {
      clientList.clear();
      notifyDataSetChanged();
    }
  }

  public void addAll(List<Client> mClientList) {
    mClientList.clear();
    mClientList.addAll(mClientList);
    notifyDataSetChanged();
  }


  public class EventHandler{
    public void onCLickTag(View view){
      Log.d(TAG, "onCLickTag");
      Object obj = view.getTag();
      if (obj instanceof String) {
        String url = (String) obj;
        if( !url.startsWith("http")){
          url = "http://" + url;
        }
        try{

          Intent i = new Intent(Intent.ACTION_VIEW);
          i.setData(Uri.parse(url));
          view.getContext().startActivity(i);

        }catch (Exception e){
          Log.e(TAG, e.toString());
        }


      }
    }
  }



  public class ClintViewHolder extends MyViewHolder{

    public ClintViewHolder(ViewDataBinding binding) {
      super(binding);

    }

    @Override
    public void doCustomBinding(ViewDataBinding binding) {
      super.doCustomBinding(binding);

      Log.d(TAG, "doCustomBinding");
      // This is cool
      int position = getAdapterPosition();
      Client client = clientList.get(position);
      List<TagData> tagList = client.getTagList();

      if (tagList != null) {
        LinearLayout linearLayout =
            (LinearLayout) binding.getRoot().findViewById(R.id.linearLayoutTags);
        linearLayout.removeAllViews();
        LayoutInflater layoutInflater = LayoutInflater.from(linearLayout.getContext());
        for (TagData tagData : tagList) {

          TextViewTagBinding
              tagDataBinding = DataBindingUtil.inflate(layoutInflater, R.layout.text_view_tag, linearLayout, false);

          tagDataBinding.setEventHandler(eventHandler);

          TextView textView = tagDataBinding.textViewTag;
          textView.setText(tagData.getTag());
          textView.setTag(tagData.getUrl());
          linearLayout.addView(tagDataBinding.getRoot());

        }
      }

    }
  }



}

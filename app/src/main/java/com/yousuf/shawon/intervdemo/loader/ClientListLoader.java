package com.yousuf.shawon.intervdemo.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import com.yousuf.shawon.intervdemo.BuildConfig;
import com.yousuf.shawon.intervdemo.MyApplication;
import com.yousuf.shawon.intervdemo.model.Client;
import com.yousuf.shawon.intervdemo.model.ClientResponse;
import com.yousuf.shawon.intervdemo.rest.ApiClient;
import com.yousuf.shawon.intervdemo.rest.ApiInterface;
import com.yousuf.shawon.intervdemo.util.NetworkUtil;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Yousuf on 6/15/2017.
 */

public class ClientListLoader extends AsyncTaskLoader<ClientResponse> {

  ClientResponse mData;
  String TAG = getClass().getSimpleName();

  public ClientListLoader(Context context) {
    super(context);
  }

  @Override protected void onStartLoading() {
    Log.d(TAG, "onStartLoading");
    if (mData!= null) {
      deliverResult(mData);
    }else {

      if( MyApplication.responseData != null ){
        deliverResult(MyApplication.responseData);
      }else {
        forceLoad();
      }

    }
  }

  @Override public ClientResponse loadInBackground() {
    Log.d(TAG, "loadInBackground");

    if (MyApplication.responseData != null) {

      return MyApplication.responseData;

    }else {
      return  NetworkUtil.getResponseSync();
    }

  }

  @Override public void deliverResult(ClientResponse data) {
    Log.d(TAG, "deliverResult");

    if (isReset()) {
      mData = null;
      return;
    }

    ClientResponse oldData = mData;
    mData = data;

    if (isStarted()) {
      super.deliverResult(data);
    }

    if (oldData != null && oldData != data) {
      oldData = null;
    }

    super.deliverResult(data);

  }

  @Override protected void onStopLoading() {
    Log.d(TAG, "onStopLoading");
    cancelLoad();
  }

  @Override protected void onReset() {
    Log.d(TAG, "onReset");
    onStopLoading();
    if (mData != null) {
      mData = null;
    }
  }

  @Override public void onCanceled(ClientResponse data) {
    Log.d(TAG, "onCanceled");
    super.onCanceled(data);
    data = null;
  }
}

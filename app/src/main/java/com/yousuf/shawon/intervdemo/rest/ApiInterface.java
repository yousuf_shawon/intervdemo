package com.yousuf.shawon.intervdemo.rest;

import com.yousuf.shawon.intervdemo.model.ClientResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Yousuf on 6/15/2017.
 */

public interface ApiInterface {

  @GET("client") Call<ClientResponse> getData( @Header("authorization") String key);
}

package com.yousuf.shawon.intervdemo.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Yousuf on 6/15/2017.
 */

public class Client {

  @SerializedName("name")
  String name;
  @SerializedName("logo")
  String logoUrl;
  @SerializedName("company")
  String company;
  @SerializedName("country")
  String country;
  @SerializedName("tags")
  ArrayList<TagData> tagList;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public ArrayList<TagData> getTagList() {
    return tagList;
  }

  public void setTagList(ArrayList<TagData> tagList) {
    this.tagList = tagList;
  }

  @Override public String toString() {
    return "Client{"
        + "name='"
        + name
        + '\''
        + ", logoUrl='"
        + logoUrl
        + '\''
        + ", company='"
        + company
        + '\''
        + ", country='"
        + country
        + '\''
      //  + ", tagList="
      //  + tagList == null ? "null" : Arrays.toString(tagList.toArray())
        + '}';
  }
}

package com.yousuf.shawon.intervdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {

  Button button1, button2;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dashboard);

    button1 = (Button) findViewById(R.id.button1);
    button2 = (Button) findViewById(R.id.button2);

    button1.setOnClickListener(this);
    button2.setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {

    switch (view.getId()) {
      case R.id.button1:
        startActivity(new Intent(this, MapsActivity.class));
        break;

      case R.id.button2:
        startActivity(new Intent(this, ClientActivity.class));
        break;

      default:
        break;
    }
  }
}

package com.yousuf.shawon.intervdemo;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yousuf.shawon.intervdemo.databinding.ActivityMapsBinding;
import java.util.ArrayList;
import java.util.List;


public class MapsActivity extends AppCompatActivity
    implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener{

  private GoogleMap mMap;


  // The entry point to Google Play services, used by the Places API and Fused Location Provider.
  private GoogleApiClient mGoogleApiClient;

  private CameraPosition mCameraPosition;

  // A default location (Sydney, Australia) and default zoom to use when location permission is
  // not granted.
  private final LatLng mDefaultLocation = new LatLng(23.7518961,90.3812769);
  private static final int DEFAULT_ZOOM = 15;
  private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
  private boolean mLocationPermissionGranted;

  // The geographical location where the device is currently located. That is, the last-known
  // location retrieved by the Fused Location Provider.
  private Location mLastKnownLocation;

  // Keys for storing activity state.
  private static final String KEY_CAMERA_POSITION = "camera_position";
  private static final String KEY_LOCATION = "location";

  private double offset = 0.005;

  private double mXOffset[] = {offset, -offset, 0, 0};
  private double mYOffset[] = {0, 0, offset, -offset};

  private int[] transportResourceIds = {R.drawable.mini, R.drawable.sub, R.drawable.micro, R.drawable.bike, R.drawable.nova };
  String transportNames[];
  List<Marker> usedMarkerList;

  ImageView imageViewMyLocation, imageViewExpand;
  LinearLayout linearLayoutTransport, linearLayoutTransportOptions;
  ImageView []imageViewTransports;
  Toolbar toolbar;

  private String TAG = getClass().getSimpleName();

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
   // setContentView(R.layout.activity_maps);

    ActivityMapsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);

    binding.setEventHandler(new EventHandler());

    setupActionBar();

    transportNames = getResources().getStringArray(R.array.transport_names);

    // initializing UI
    imageViewTransports = new ImageView[5];


    imageViewMyLocation = binding.imageViewMyLocation;
    imageViewExpand = binding.imageViewExpand;
    linearLayoutTransportOptions = binding.linearLayoutTransportOption;
    linearLayoutTransport = binding.linearLayoutTransport;


    View transportView[] = new View[5];
    transportView[0] = findViewById(R.id.transport1);
    transportView[1] = findViewById(R.id.transport2);
    transportView[2] = findViewById(R.id.transport3);
    transportView[3] = findViewById(R.id.transport4);
    transportView[4] = findViewById(R.id.transport5);





    for (int i = 0; i < 5; i++) {
      imageViewTransports[i] = (ImageView) transportView[i].findViewById(R.id.imageViewTrans);
      TextView textView= (TextView) transportView[i].findViewById(R.id.textView);
      textView.setText(transportNames[i]);
      imageViewTransports[i].setImageResource( transportResourceIds[i] );
      imageViewTransports[i].setTag(i);
    }


    // Build the Play services client for use by the Fused Location Provider and the Places API.
    // Use the addApi() method to request the Google Places API and the Fused Location Provider.
    mGoogleApiClient =
        new GoogleApiClient.Builder(this).enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
            .addConnectionCallbacks(this)
            .addApi(LocationServices.API)
            .addApi(Places.GEO_DATA_API)
            .addApi(Places.PLACE_DETECTION_API)
            .build();
    mGoogleApiClient.connect();




  }


  /**
   * Saves the state of the map when the activity is paused.
   */
  @Override
  protected void onSaveInstanceState(Bundle outState) {
    if (mMap != null) {
      outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
      outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
      super.onSaveInstanceState(outState);
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    mGoogleApiClient.connect();
  }

  @Override
  public void onStop() {
    super.onStop();
    if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
      mGoogleApiClient.disconnect();
    }
  }

  private void setupActionBar(){
    toolbar = (Toolbar) findViewById(R.id.appBar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setTitle("Confirmation");
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
  }




  /**
   * Manipulates the map once available.
   * This callback is triggered when the map is ready to be used.
   * This is where we can add markers or lines, add listeners or move the camera. In this case,
   * we just add a marker near Sydney, Australia.
   * If Google Play services is not installed on the device, the user will be prompted to install
   * it inside the SupportMapFragment. This method will only be triggered once the user has
   * installed Google Play services and returned to the app.
   */
  @Override public void onMapReady(GoogleMap googleMap) {

    Log.d(TAG, "onMapReady");
    mMap = googleMap;

    //// Add a marker in Sydney and move the camera
    //LatLng sydney = new LatLng(-34, 151);
    //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
    //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    // Turn on the My Location layer and the related control on the map.
    updateLocationUI();

    // Get the current location of the device and set the position of the map.
    getDeviceLocation();
  }

  @Override public void onConnected(@Nullable Bundle bundle) {
    Log.d(TAG, "onConnected");

    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
        (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
  }

  @Override public void onConnectionSuspended(int i) {
    Log.d(TAG, "onConnectionSuspended");
    Log.d(TAG, "Play services connection suspended");
  }

  /**
   * Handles failure to connect to the Google Play services client.
   */
  @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.d(TAG, "onConnectionFailed");
    // Refer to the reference doc for ConnectionResult to see what error codes might
    // be returned in onConnectionFailed.
    Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
        + connectionResult.getErrorCode());
  }

  /**
   * Handles the result of the request for location permissions.
   */
  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
      @NonNull int[] grantResults) {

    mLocationPermissionGranted = false;
    switch (requestCode) {
      case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mLocationPermissionGranted = true;
        }
      }
    }

    if (mLocationPermissionGranted) {
      updateLocationUI();
      // Get the current location of the device and set the position of the map.
      getDeviceLocation();
    }

  }

  /**
   * Updates the map's UI settings based on whether the user has granted location permission.
   */
  private void updateLocationUI() {
    if (mMap == null) {
      return;
    }

        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
    if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      mLocationPermissionGranted = true;
    } else {
      ActivityCompat.requestPermissions(this,
          new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
          PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }

    if (mLocationPermissionGranted) {
      mMap.setMyLocationEnabled(true);
     // mMap.getUiSettings().setMyLocationButtonEnabled(true);
    } else {
      mMap.setMyLocationEnabled(false);
     // mMap.getUiSettings().setMyLocationButtonEnabled(false);
      mLastKnownLocation = null;
    }
  }

  /**
   * Gets the current location of the device, and positions the map's camera.
   */
  private void getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
    if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      mLocationPermissionGranted = true;
    } else {
      ActivityCompat.requestPermissions(this,
          new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
          PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
    if (mLocationPermissionGranted) {
      mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    LatLng displayLatLng = null;

    // Set the map's camera position to the current location of the device.
    if (mCameraPosition != null) {
      mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
      displayLatLng = new LatLng(mCameraPosition.target.latitude,mCameraPosition.target.longitude );

    } else if (mLastKnownLocation != null) {
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
          new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()),
          DEFAULT_ZOOM));
      displayLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
    } else {
      Log.d(TAG, "Current location is null. Using defaults.");
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
      mMap.getUiSettings().setMyLocationButtonEnabled(false);

      mMap.addMarker(new MarkerOptions().position(mDefaultLocation).title("Marker in Default Location"));


      displayLatLng = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);

    }


    // Displaying Random Marker
    displayRandomMarker(displayLatLng);

  }


  private void displayRandomMarker(LatLng displayLatLng){

    if (mMap == null) {
      return;
    }

    List<MarkerOptions> markerOptionsList = getMarkerOptionsList(displayLatLng);
    Log.d(TAG, "Adding " + markerOptionsList.size() + " markers");

    removeMarkers();

    if (usedMarkerList == null) {
      usedMarkerList = new ArrayList<>();
    }

    for (MarkerOptions markerOptions : markerOptionsList) {
      Marker marker = mMap.addMarker(markerOptions);
      usedMarkerList.add(marker);
    }

  }


  private List<MarkerOptions> getMarkerOptionsList(LatLng displayLatLng){
    List<MarkerOptions> markerOptionsList = new ArrayList<>();
    if (displayLatLng != null) {

      for (int i =0; i<mXOffset.length; i++ ){

        Log.d(TAG, "Latt: " +(displayLatLng.latitude + mXOffset[i])  + " Lang: " +  (displayLatLng.longitude + mYOffset[i]) );

        MarkerOptions markerOptions = new MarkerOptions()
            .position(new LatLng(displayLatLng.latitude + mXOffset[i], displayLatLng.longitude + mYOffset[i]))
            .title(String.valueOf(i));
        markerOptions.icon( BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource( getResources(),
            R.drawable.car ) ) );

        markerOptionsList.add(markerOptions);

      }

    }

    return markerOptionsList;
  }


  private void removeMarkers(){
    if (usedMarkerList != null) {
      for (Marker marker : usedMarkerList) {
        marker.remove();
      }

      usedMarkerList.clear();
    }
  }

  private void onMyLocationClick(){

    loadMyLocation();

  }

  private void onOptionExpandClick(){

    if (linearLayoutTransport.getVisibility() == View.GONE) {
      // expand
      linearLayoutTransport.setVisibility(View.VISIBLE);
      linearLayoutTransportOptions.setVisibility(View.VISIBLE);
      imageViewExpand.setImageResource(R.drawable.ic_expand_more);


    }else {
      // collapse
      linearLayoutTransport.setVisibility(View.GONE);
      linearLayoutTransportOptions.setVisibility(View.GONE);
      imageViewExpand.setImageResource(R.drawable.ic_expand_less);
    }

  }

  private void onTransPortClick(ImageView imageView){
    resetTransportBackground();
    imageView.setBackgroundResource(R.drawable.circle_blue);
  }



  private void loadMyLocation(){

    Log.d(TAG, "Loading my location");
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
    if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      mLocationPermissionGranted = true;
    } else {
      ActivityCompat.requestPermissions(this,
          new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
          PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
    if (mLocationPermissionGranted) {
      mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    LatLng displayLatLng = null;

    if (mLastKnownLocation != null) {
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
          new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()),
          DEFAULT_ZOOM));
      displayLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
    }

    // Displaying Random Marker
    displayRandomMarker(displayLatLng);

  }


  private void resetTransportBackground(){
    for (int i = 0; i < 5; i++) {
      imageViewTransports[i].setBackgroundResource(R.drawable.circle_gray);
    }
  }


  public class EventHandler{

    public void onClick(View v){

      switch (v.getId()) {
        case R.id.imageViewMyLocation:
          onMyLocationClick();
          break ;

        case R.id.imageViewExpand:
          onOptionExpandClick();
          break ;

        case R.id.imageViewTrans:
          Log.d(TAG, "transPortClick");
          onTransPortClick((ImageView)v);
          break ;

        default:
          break;
      }

    }
  }




}
